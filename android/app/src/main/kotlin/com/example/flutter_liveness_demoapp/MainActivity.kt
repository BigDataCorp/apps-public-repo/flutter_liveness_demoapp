package com.example.flutter_liveness_demoapp

import android.content.Intent
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterFragmentActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterFragmentActivity() {
    private val CHANNEL = "flutter.native/helper"
    private val RQ_LIVENESS = 0


    @ExperimentalStdlibApi
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler{
                call, result ->
            when {
                call.method.equals("navigation") -> {
                    val it = Intent(this, NavigationActivity::class.java)
                    startActivityForResult(it, RQ_LIVENESS)
                }
            }
        }
    }

    companion object {
        const val TAG = "MainActivity"
    }

}
