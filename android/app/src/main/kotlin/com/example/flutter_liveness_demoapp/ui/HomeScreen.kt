package com.example.flutter_liveness_demoapp.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.biometrics.kotlinlivenesssdk.LivenessMainViewModel
import com.example.flutter_liveness_demoapp.R
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun HomeScreen(
    viewModel: LivenessMainViewModel,
    onStartChallenge: (sessionId: String) -> Unit
) {
    val cameraPermissionState = rememberPermissionState(Manifest.permission.CAMERA)
    val fetchingSession = viewModel.fetchingSession.collectAsState().value
    val createResult = viewModel.createResult.collectAsState().value
    val token = stringResource(R.string.TOKEN)
    val showAlertMessage = remember {
        mutableStateOf((createResult != null) && (createResult.resultCode != 1301))
    }
    LaunchedEffect(Unit) {
        viewModel.createLivenessSession(token) { sessionId ->
            sessionId?.let { onStartChallenge(it) }?: run { showAlertMessage.value = true }
        }
    }
    Row(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(16.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        when {
            cameraPermissionState.status.shouldShowRationale -> {
                Text(stringResource(R.string.open_settings_camera_permissions))
            }
            !cameraPermissionState.status.isGranted -> {
                SideEffect {
                    cameraPermissionState.launchPermissionRequest()
                }
                Button(onClick = { cameraPermissionState.launchPermissionRequest() }) {
                    Text(stringResource(R.string.grant_camera_permission))
                }
            }
            fetchingSession -> {
                CircularProgressIndicator()
            }
            showAlertMessage.value -> {
                AlertDialog(
                    onDismissRequest = { showAlertMessage.value = false },
                    title = {Text(text=createResult!!.resultCode.toString())},
                    text = { Text(text = createResult!!.resultMessage!!)},
                    confirmButton = { Button(onClick = { showAlertMessage.value = false }) {
                        Text(text = "OK")
                    }}
                )
            }
            else -> {
                Button(
                    onClick = {
                        viewModel.createLivenessSession(token) { sessionId ->
                            sessionId?.let { onStartChallenge(it) }?: run { showAlertMessage.value = true }
                        }
                    }
                ) {
                    Text(stringResource(R.string.create_liveness_session))
                }
            }
        }
    }
}

fun Context.findActivity(): Activity? = when (this) {
    is Activity -> this
    is ContextWrapper -> baseContext.findActivity()
    else -> null
}